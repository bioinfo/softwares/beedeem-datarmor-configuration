db.name=Genbank_FishDNA12S
db.desc=Genbank 12S sequences from Chondrichthyes, Actinopteri, Actinopterygii
db.type=n
db.ldir=${mirrordir}|n|Genbank_FishDNA12S
db.provider=SeBiMER

db.files.include=12S-fish.fasta
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

# singularity use:
local.rdir=${mirrordir}/lcl-download/EGG_12S_FISH
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

