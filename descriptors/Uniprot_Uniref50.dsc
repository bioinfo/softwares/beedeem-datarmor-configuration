db.name=Uniprot_Uniref50
db.desc=Uniprot TrEMBL/SwissProt non-redundant, all organisms from UniProt/Uniref 50 databank.
db.type=p
db.ldir=${mirrordir}|p|Uniprot_Uniref50
db.provider=EBI

db.files.include=uniref50.fasta.gz
db.files.exclude=

tasks.unit.post=gunzip,idxfas
tasks.global.post=formatdb(lclid=true;check=true;nr=true),script(name=GetUP;path=get_up_release),script(name=DiamondIndex;path=run_diamond),delgz,deltmpidx

ftp.server=ftp.expasy.org
ftp.port=21
ftp.uname=anonymous
ftp.pswd=user@company.com
ftp.rdir=/databases/uniprot/current_release/uniref/uniref50
ftp.rdir.exclude=

history=2

