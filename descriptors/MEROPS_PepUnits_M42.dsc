db.name=MEROPS_Pep_Units_M42
db.desc=Library of M42 peptidase units extracted from full MEROPS_Pep_Units bank 
db.type=p
db.ldir=${mirrordir}|p|MEROPS_Pep_Units_M42
db.provider=SeBiMER

db.files.include=merops_M42.fas
db.files.exclude=

tasks.unit.post=idxfas
tasks.global.post=deltmpidx,formatdb(lclid=false;check=true;nr=true)

local.rdir=/home/ref-bioinfo/ifremer/sebimer/generic-reference-banks/lcl-download
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=0


