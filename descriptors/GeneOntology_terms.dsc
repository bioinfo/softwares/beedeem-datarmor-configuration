db.name=GeneOntology_terms
db.desc=Gene Ontology Terms (GO basic)
db.type=d
db.ldir=${mirrordir}|d|GeneOntology_terms
db.provider=GO Consortium

db.files.include=go-basic.obo
db.files.exclude=

tasks.global.pre=script(name=GetGO;path=get_gene_ontology)

tasks.unit.post=idxdico(type=go)
tasks.global.post=deltmpidx,script(name=GetGO;path=get_go_release)

local.rdir=${workdir}|GeneOntology_terms
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=8

