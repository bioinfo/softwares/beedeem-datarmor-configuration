db.name=Refseq_protein_archaea
db.desc=Refseq Archaea subset Protein databank
db.type=p
db.ldir=${mirrordir}|p|Refseq_protein_archaea
db.provider=NCBI

db.files.include=^archaea.nonredundant_protein.*\\d+\\.protein.gpff.gz$
db.files.exclude=

tasks.unit.post=gunzip,idxgp
tasks.global.post=formatdb(lclid\=false;check\=true;nr\=true),script(name=GetREF;path=get_ref_release),script(name=DiamondIndex;path=run_diamond),delgz,deltmpidx

ftp.server=ftp.ncbi.nih.gov
ftp.port=21
ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.rdir=/refseq/release/archaea/
ftp.rdir.exclude=
ftp.alt.protocol=https

history=0

aspera.use=false
aspera.server=anonftp@ftp.ncbi.nlm.nih.gov
aspera.args=-k 1 -T -l 640M

