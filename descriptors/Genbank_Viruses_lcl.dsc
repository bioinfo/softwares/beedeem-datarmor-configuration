db.name=Genbank_Viruses
db.desc=Genbank Virus (VRL) sequences
db.type=n
db.ldir=${mirrordir}|n|Genbank_Viruses
db.provider=NCBI

db.files.include=^gbvrl.*\\.seq$
db.files.exclude=

tasks.unit.post=

tasks.global.post=formatdb(lclid=false;check=true;nr=true)

#Singularity use:
local.rdir=${mirrordir}/n/Genbank_CoreNucleotide/current/Genbank_CoreNucleotide
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

