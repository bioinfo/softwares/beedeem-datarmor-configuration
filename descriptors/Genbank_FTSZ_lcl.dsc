db.name=Genbank_FTSZ
db.desc=Genbank FtsZ sequences, all organisms
db.type=n
db.ldir=${mirrordir}|n|Genbank_FTSZ
db.provider=SeBiMER

db.files.include=ftsz.fasta
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

#Singularity use:
local.rdir=${mirrordir}/lcl-download/EGG_FTSZ
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

