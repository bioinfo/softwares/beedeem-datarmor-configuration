db.name=Genbank_COI
db.desc=Genbank Cytochrome Oxidase I sequences, all organisms
db.type=n
db.ldir=${mirrordir}|n|Genbank_COI
db.provider=SeBiMER

db.files.include=coi.fasta
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

#Singularity use:
local.rdir=${mirrordir}/lcl-download/EGG_COI
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

