db.name=Uniprot_SwissProt
db.desc=UniprotKB/SwissProt databank
db.type=p
db.ldir=${mirrordir}|p|Uniprot_SwissProt
db.provider=EBI

db.files.include=uniprot_sprot.dat.gz
db.files.exclude=

tasks.unit.post=gunzip,idxsw
tasks.global.post=formatdb(lclid=false;check=true;nr=true),script(name=GetUP;path=get_up_release),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),delgz,deltmpidx

ftp.server=ftp.expasy.org
ftp.port=21
ftp.uname=anonymous
ftp.pswd=user@company.com
ftp.rdir=/databases/uniprot/current_release/knowledgebase/complete
ftp.rdir.exclude=

history=2

