db.name=MIDORI_12S_longest
db.desc=MIDORI 12S longest sequences
db.type=n
db.ldir=${mirrordir}|n|MIDORI_12S_longest
db.provider=MIDORI

db.files.include=.*\\.zip$
db.files.exclude=

tasks.global.pre=script(name=GetMIDORI_12S_longest;path=get_midori_12S_long)

tasks.unit.post=gunzip
tasks.global.post=makealias,script(name=GetMIDORI_12S_longest_other;path=get_midori_12S_long_other),script(name=RDP_classifier;path=run_rdptrain_midori)

local.rdir=${workdir}|MIDORI_12S_longest
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2


