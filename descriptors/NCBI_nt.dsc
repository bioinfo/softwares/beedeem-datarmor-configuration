db.name=NCBI_nt
db.desc=NCBI nt nucleotide databank with taxonomy. Nucleotide sequences from several sources, including GenBank, RefSeq, TPA and PDB. Redundant. 
db.type=n
db.ldir=${mirrordir}|n|NCBI_nt
db.provider=NCBI

db.files.include=^nt.*\\d+\\.tar.gz$
db.files.exclude=

tasks.unit.post=gunzip,untar
tasks.global.post=makealias,script(name=DumpFasta;path=run_blastcmd),delgz,deltar

ftp.server=ftp.ncbi.nih.gov
ftp.port=21
ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.rdir=/blast/db
ftp.rdir.exclude=
#ftp.alt.protocol=https

history=2

aspera.use=false
aspera.server=anonftp@ftp.ncbi.nlm.nih.gov
aspera.args=-k 1 -T -l 640M


