#PDB_proteins
#Fri Sep 15 13:47:28 CEST 2017

db.name=PDB_proteins
db.desc=PDB Protein databank 
db.type=p
db.ldir=${mirrordir}|p|PDB_proteins
db.provider=NCBI

db.files.include=pdbaa.tar.gz
db.files.exclude=

tasks.unit.post=gunzip,untar

tasks.global.post=makealias,script(name=DumpFasta;path=run_blastcmd),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),delgz,deltar

ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.server=ftp.ncbi.nih.gov
ftp.port=21
ftp.rdir=/blast/db
ftp.rdir.exclude=
#ftp.alt.protocol=https

depends=

history=2

aspera.use=false
aspera.server=anonftp@ftp.ncbi.nlm.nih.gov
aspera.args=-k 1 -T -l 640M

