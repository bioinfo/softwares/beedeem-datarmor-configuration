db.name=MIDORI_12S_uniq
db.desc=MIDORI Cytochrome b unique sequences
db.type=n
db.ldir=${mirrordir}|n|MIDORI_12S_uniq
db.provider=MIDORI

db.files.include=.*\\.zip$
db.files.exclude=

tasks.global.pre=script(name=GetMIDORI_12S_uniq;path=get_midori_12S_uniq)

tasks.unit.post=gunzip
tasks.global.post=makealias,script(name=GetMIDORI_12S_uniq_other;path=get_midori_12S_uniq_other),script(name=RDP_classifier;path=run_rdptrain_midori)

local.rdir=${workdir}|MIDORI_12S_uniq
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2


