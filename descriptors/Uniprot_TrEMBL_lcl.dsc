db.name=Uniprot_TrEMBL
db.desc=UniprotKB/TrEMBL databank
db.type=p
db.ldir=${mirrordir}|p|Uniprot_TrEMBL
db.provider=EBI

db.files.include=uniprot_trembl.dat.gz
db.files.exclude=

tasks.unit.post=gunzip,idxsw
tasks.global.post=formatdb(lclid=false;check=true;nr=true),script(name=GetUP;path=get_up_release),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),deltmpidx

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

local.rdir=${mirrordir}/lcl-download
local.rdir.exclude=

history=2

