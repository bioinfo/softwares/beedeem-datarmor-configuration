db.name=UniVec
db.desc=UniVec database
db.type=n
db.ldir=${mirrordir}|n|UniVec
db.provider=NCBI

db.files.include=UniVec
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

ftp.server=ftp.ncbi.nih.gov
ftp.port=21
ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.rdir=/pub/UniVec
ftp.rdir.exclude=
ftp.alt.protocol=https

history=0

