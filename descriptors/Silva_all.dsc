db.name=Silva_all
db.desc=Silva LSU+SSU Reference sequence databank
db.type=n
db.ldir=${mirrordir}|n|Silva_all
db.provider=SeBiMER

db.files.include=Silva_LSU00,Silva_SSU00
db.files.exclude=

#tasks.global.pre=script(name=GetSilvaLSU;path=get_silva_lsu)

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true),script(name=GetLSURelease;path=copy_silva_release),script(name=BuildIndex;path=post_process_silva),delgz,deltmpidx

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

local.rdir=/home/ref-bioinfo/ifremer/sebimer/generic-reference-banks/n/Silva_SSU/current/Silva_SSU,/home/ref-bioinfo/ifremer/sebimer/generic-reference-banks/n/Silva_LSU/current/Silva_LSU
local.rdir.exclude=

history=0

