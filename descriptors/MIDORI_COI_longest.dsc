db.name=MIDORI_COI_longest
db.desc=MIDORI COI longest sequences
db.type=n
db.ldir=${mirrordir}|n|MIDORI_COI_longest
db.provider=MIDORI

db.files.include=.*\\.zip$
db.files.exclude=

tasks.global.pre=script(name=GetMIDORI_COI_longest;path=get_midori_COI_long)

tasks.unit.post=gunzip
tasks.global.post=makealias,script(name=GetMIDORI_COI_longest_other;path=get_midori_COI_long_other),script(name=RDP_classifier;path=run_rdptrain_midori)

local.rdir=${workdir}|MIDORI_COI_longest
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2


