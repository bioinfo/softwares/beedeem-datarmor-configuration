db.name=TCDB
db.desc=Transporter Classification Database
db.type=p
db.ldir=${mirrordir}|p|TCDB
db.provider=TCDB.org

db.files.include=tcdb.fasta
db.files.exclude=

tasks.global.pre=script(name=GetTCDB;path=get_tcdb)

tasks.unit.post=idxfas
tasks.global.post=formatdb(lclid=true;check=true;nr=true),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),deltmpidx

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

local.rdir=${workdir}|TCDB
local.rdir.exclude=

history=0

