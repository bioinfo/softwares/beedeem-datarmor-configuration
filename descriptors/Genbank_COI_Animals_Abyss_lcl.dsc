db.name=Genbank_COI_Animals_Abyss
db.desc=Genbank Cytochrome Oxidase I sequences, animals for Abyss project
db.type=n
db.ldir=${mirrordir}|n|Genbank_COI_Animals_Abyss
db.provider=SeBiMER

db.files.include=coi_animals_abyss.fasta
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

#Singularity use:
local.rdir=${mirrordir}/lcl-download/EGG_COI_Animals_Abyss
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

