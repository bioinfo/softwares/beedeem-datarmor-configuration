db.name=SwissProt_human
db.desc=Human subset of UniprotKB/SwissProt
db.type=p
db.ldir=${mirrordir}|p|SwissProt_human
db.provider=EBI

db.files.include=uniprot_sprot_human.dat.gz
db.files.exclude=

tasks.unit.post=gunzip,idxsw
tasks.global.post=formatdb(lclid\=false;check\=true;nr\=true),script(name=GetUP;path=get_up_release),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),delgz,deltmpidx

ftp.server=ftp.expasy.org
ftp.port=21
ftp.uname=anonymous
ftp.pswd=user@company.com
ftp.rdir=/databases/uniprot/current_release/knowledgebase/taxonomic_divisions
ftp.rdir.exclude=

history=2

