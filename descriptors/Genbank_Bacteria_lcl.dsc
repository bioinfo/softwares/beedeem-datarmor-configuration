db.name=Genbank_Bacteria
db.desc=Genbank Bacteria (BCT) sequences
db.type=n
db.ldir=${mirrordir}|n|Genbank_Bacteria
db.provider=NCBI

db.files.include=^gbbct.*\\.seq$
db.files.exclude=

tasks.unit.post=
tasks.global.post=formatdb(lclid=false;check=true;nr=true)

local.rdir=${mirrordir}/n/Genbank_CoreNucleotide/current/Genbank_CoreNucleotide
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2

