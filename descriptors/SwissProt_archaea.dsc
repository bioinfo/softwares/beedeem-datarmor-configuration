db.name=SwissProt_archaea
db.desc=Archaea subset of UniprotKB/SwissProt
db.type=p
db.ldir=${mirrordir}|p|SwissProt_archaea
db.provider=SeBiMER

db.files.include=uniprot_sprot.dat
db.files.exclude=

tasks.unit.post=idxsw(taxinc\=2157)
tasks.global.post=formatdb(lclid\=false;check\=true;nr\=true;taxinc\=2157),script(name=DiamondIndex;path=run_diamond),deltmpidx

local.rdir=/home/ref-bioinfo/ifremer/sebimer/generic-reference-banks/p/Uniprot_SwissProt/current/Uniprot_SwissProt
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=0
