db.name=NCBI_nr_archaea
db.desc=NCBI_nr Archaea subset databank
db.type=p
db.ldir=${mirrordir}|p|NCBI_nr_archaea
db.provider=SeBiMER

db.files.include=archaea.faa.gz
db.files.exclude=

tasks.global.pre=script(name=DumpArchaeaNR;path=run_nr_archaea_dump)

tasks.unit.post=gunzip,idxfas
tasks.global.post=formatdb(lclid=false;check=true;nr=true),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),delgz,deltmpidx

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

# Working directory expected by run_nr_archaea_dump.sh pre-processing script
local.rdir=${workdir}|NCBI_nr_archaea
local.rdir.exclude=

history=2

