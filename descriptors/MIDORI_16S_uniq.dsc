db.name=MIDORI_16S_uniq
db.desc=MIDORI Cytochrome b unique sequences
db.type=n
db.ldir=${mirrordir}|n|MIDORI_16S_uniq
db.provider=MIDORI

db.files.include=.*\\.zip$
db.files.exclude=

tasks.global.pre=script(name=GetMIDORI_16S_uniq;path=get_midori_16S_uniq)

tasks.unit.post=gunzip
tasks.global.post=makealias,script(name=GetMIDORI_16S_uniq_other;path=get_midori_16S_uniq_other),script(name=RDP_classifier;path=run_rdptrain_midori)

local.rdir=${workdir}|MIDORI_16S_uniq
local.rdir.exclude=

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

history=2


