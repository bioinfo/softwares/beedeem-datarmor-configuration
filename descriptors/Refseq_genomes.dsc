db.name=Refseq_genomes
db.desc=Refseq complete Genomes databank
db.type=n
db.ldir=${mirrordir}|n|Refseq_genomes
db.provider=NCBI

db.files.include=^complete.*\\.genomic.fna.gz$
db.files.exclude=

tasks.unit.post=gunzip,idxfas
tasks.global.post=formatdb(lclid\=false;check\=true;nr\=true),script(name=GetREF;path=get_ref_release),script(name=RunMinimap2;path=run_minimap2),delgz,deltmpidx

ftp.server=ftp.ncbi.nih.gov
ftp.port=21
ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.rdir=/refseq/release/complete/
ftp.rdir.exclude=
ftp.alt.protocol=https

history=2

aspera.use=true
aspera.server=anonftp@ftp.ncbi.nlm.nih.gov
aspera.args=-k 1 -T -l 640M

