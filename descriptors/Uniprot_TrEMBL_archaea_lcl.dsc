db.name=Uniprot_TrEMBL_archaea
db.desc=UniprotKB/TrEMBL Archaea subset databank
db.type=p
db.ldir=${mirrordir}|p|Uniprot_TrEMBL_archaea
db.provider=SeBiMER

db.files.include=archaea_sw.faa,archaea_tr.faa
db.files.exclude=

#of note: will have to automate the use of script
# <home-galaxy>/beedeem/bdm-trembl-archaea.pbs
#here as a pre-processing task

tasks.unit.post=idxfas
tasks.global.post=formatdb(lclid=true;check=true;nr=true),script(name=DiamondIndex;path=run_diamond),delgz,deltmpidx

ftp.server=
ftp.port=
ftp.uname=
ftp.pswd=
ftp.rdir=
ftp.rdir.exclude=

local.rdir=${mirrordir}/lcl-download
local.rdir.exclude=

history=0

