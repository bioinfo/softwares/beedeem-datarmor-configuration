db.name=NCBI_nr
db.desc=NCBI nr protein databank with taxonomy. Protein sequences from GenBank CDS translations, PDB, Swiss-Prot, PIR, and PRF.
db.type=p
db.ldir=${mirrordir}|p|NCBI_nr
db.provider=NCBI

db.files.include=^nr.*\\d+\\.tar.gz$
db.files.exclude=

tasks.unit.post=gunzip,untar
tasks.global.post=makealias,script(name=DumpFasta;path=run_blastcmd),script(name=DiamondIndex;path=run_diamond),delgz,deltar
# The following does not work anymore: error while preparing BLAST v4 bank (wrong seqIDs)
#tasks.global.post=makealias,script(name=DumpFasta;path=run_blastcmd),script(name=DiamondIndex;path=run_diamond),script(name=Blastv4Index;path=run_makeblastdb),delgz,deltar

ftp.server=165.112.9.228
ftp.port=21
ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.rdir=/blast/db
ftp.rdir.exclude=
#ftp.alt.protocol=https

history=2

aspera.use=false
aspera.server=anonftp@ftp.ncbi.nlm.nih.gov
aspera.args=-k 1 -T -l 640M


