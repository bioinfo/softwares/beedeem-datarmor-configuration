## About

This repository contains the configuration directory (descriptors, processing scripts and job scheduler configuration) used by [SeBiMER](https://bioinfo.ifremer.fr) to manage bioinformatics banks on the DATARMOR supercomputer hosted at Ifremer.

This is actually a complete 'conf' directory to be used with the [BeeDeeM software](https://github.com/pgdurand/BeeDeeM).

## License

This is free and open-source software covered by Affero-GPL.

## Credits

2017-2023 by SeBiMER
