#!/usr/bin/env bash

# This is a BeeDeeM external task script aims at fastly
# uncompressinng huge TrEMBL bank.
#
# This script is used in bank descriptor:
#   ../descriptors/Uniprot_TrEMBL.dsc
#
# Such a BeeDeeM script is called by the task engine and
# with up to three arguments: -w <path> -d <path> -f <path>
#
#  -w <path>: <path> is the working directory path.
#             provided for both unit and global tasks.
#  -d <path>: <path> is the bank installation path.
#             provided for both unit and global tasks.
#  -f <path>: <path> is the path to file under unit task processing
#             only provided with unit task.

echo "Uncompressing TrEMBL"
echo "Arguments coming from BeeDeeM are:"
echo $@
echo "----"

# Prepare arguments for processing
WK_DIR=
INST_DIR=
PROCESSED_FILE=
while getopts w:d:f: opt
do
    case "$opt" in
      w)  WK_DIR="$OPTARG";;
      d)  INST_DIR="$OPTARG";;
      f)  PROCESSED_FILE="$OPTARG";;
    esac
done
shift `expr $OPTIND - 1`
# remaining arguments, if any, are stored here
MORE_ARGS=$@

# Get nb of cores to be used with pigz
#    if KL_CORES not defined, use only 2 threads
if [  ! "$KL_CORES"  ]; then
  KL_CORES=2
fi

echo "Working dir: $WK_DIR"
echo "Install dir: $INST_DIR"
echo "Processed file: $PROCESSED_FILE"
echo "Cores to be used with pigz: $KL_CORES" 
echo "----"

echo "Uncompressing $PROCESSED_FILE"
if [ -x "$(which pigz)" ] ; then
    CMD="pigz -k -d -p $KL_CORES $PROCESSED_FILE"
elif [ -x "$(which gunzip)" ]; then
    CMD="gunzip -k $PROCESSED_FILE"
else
    echo "Could not find pigz or gunzip, please install one." >&2
    exit 1
fi

echo $CMD
eval $CMD

