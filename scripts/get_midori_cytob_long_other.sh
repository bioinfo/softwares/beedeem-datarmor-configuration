#!/usr/bin/env bash

# This is a BeeDeeM external task script aims at downloading
# MIDORI Cytochrome B longuest DB  using HTTP protocol.
#
# This script is used in bank descriptor:
#   ../descriptors/MIDORI_CytoB_longuest.dsc
#
# Such a BeeDeeM script is called by the task engine and
# with arguments as defined in: 
# ./scheduler/common.sh->handleBDMArgs() function

echo "Getting MIDORI Cytochrome B longuest additional banks"

# ========================================================================================
# Section: include API
S_NAME=$(realpath "$0")
[[ -z "$BDM_CONF_SCRIPTS" ]] && script_dir=$(dirname "$S_NAME") || script_dir=$BDM_CONF_SCRIPTS
. $script_dir/scheduler/common.sh

# ========================================================================================
# Section: handle arguments
# Function call setting BDMC_xxx variables from cmdline arguments
handleBDMArgs $@
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Wrong or missing arguments" && exit $RET_CODE

# ========================================================================================
# Section: do business
GB_VER=$(grep "version\=" $script_dir/midori_version | cut -d'=' -f2)
URL_BASE="http://www.reference-midori.info/download/Databases/GenBank${GB_VER}"

echo "release.time.stamp=Genbank_$GB_VER" > ${BDMC_INST_DIR}/release-date.txt

declare -a index_list=(
"dada2:DADA2/longest/MIDORI2_LONGEST_NUC_GB${GB_VER}_Cytb_DADA2.fasta.gz"
"rdp:RDP/longest/MIDORI2_LONGEST_NUC_GB${GB_VER}_Cytb_RDP.fasta.gz,RDP/longest/MIDORI2_NUC_GB${GB_VER}_RDP_taxon_file.txt.gz"
"qiime2:QIIME/longest/MIDORI2_LONGEST_NUC_GB${GB_VER}_Cytb_QIIME.fasta.gz,QIIME/longest/MIDORI2_LONGEST_NUC_GB${GB_VER}_Cytb_QIIME.taxon.gz"
)

for bdm_idx in "${index_list[@]}"; do
  idx_name=$(echo $bdm_idx | cut -d':' -f1)
  idx_files=$(echo $bdm_idx | cut -d':' -f2)
  echo "Getting $idx_name index for MIDORI Cytb longest"
  idx_path=$BDMC_INST_DIR/${idx_name}.idx
  echo "  creating: $idx_path"
  mkdir -p $idx_path
  RET_CODE=$?
  [ ! $RET_CODE -eq 0 ] && errorMsg "    failed to create path" && exit $RET_CODE
  cd $idx_path
  idx_files_array=($(echo $idx_files | tr "," "\n"))
  for idx_file in "${idx_files_array[@]}"; do
    url="$URL_BASE/$idx_file"
    echo "  getting: $url"
    filename=$(basename $idx_file)
    echo "       to: $filename"
    ANSWER=$(downloadFile $filename $url)
    RET_CODE=$?        
    echo $ANSWER
    [ ! $RET_CODE -eq 0 ] && errorMsg "    failed to download file" && exit $RET_CODE
    file_ext="${filename##*.}"
    [ "$file_ext" = "gz" ] && gunzip $filename
  done
done

