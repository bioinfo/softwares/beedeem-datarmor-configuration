#!/usr/bin/env bash

# This is a BeeDeeM external task script aims at downloading
# NCBI IDs mapping file. It also prepares that file toi be
# used to prepare taxonomy-based indexes for BLAST and Diamond.
#
# Such a BeeDeeM script is called by the task engine and
# with arguments as defined in: 
# ./scheduler/common.sh->handleBDMArgs() function

echo "Getting NCBI ID mapping file"

# ========================================================================================
# Section: include API
S_NAME=$(realpath "$0")
[[ -z "$BDM_CONF_SCRIPTS" ]] && script_dir=$(dirname "$S_NAME") || script_dir=$BDM_CONF_SCRIPTS
. $script_dir/scheduler/common.sh

# ========================================================================================
# Section: handle arguemnts
# Function call setting BDMC_xxx variables from cmdline arguments
handleBDMArgs $@
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Wrong or missing arguments" && exit $RET_CODE

# ========================================================================================
# Section: do business

function getDataFile() {
  filename=$1
  url="https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/$filename"
  ANSWER=$(downloadFile $filename $url)
  RET_CODE=$?
  echo $ANSWER
  [ ! $RET_CODE -eq 0 ] && errorMsg "Unable to fetch file" && exit $RET_CODE
  echo "Uncompressing $filename..."
  CMD="gunzip $filename"
  echo $CMD
  eval $CMD
  RET_CODE=$?
  [ ! $RET_CODE -eq 0 ] && errorMsg "Unable to uncompress file" && exit $RET_CODE
}

echo "Changing dir to $BDMC_INST_DIR"
cd $BDMC_INST_DIR

getDataFile pdb.accession2taxid.gz
getDataFile prot.accession2taxid.gz
getDataFile nucl_gb.accession2taxid.gz

# Now, prepare mapping files for BLAST and Diamond

# BLAST requires a two column file WITHOUT header
echo "Preparing Protein taxID mapping file for BLAST makeblastdb tool"
DATA_FILE="prot.accession2taxid"
MAPPING_FILE="ncbi-prot-2columns.accession2taxid"
echo "  $MAPPING_FILE"
tail -n +2 $DATA_FILE | awk '{print $1,"\t",$3}' > $MAPPING_FILE
RET_CODE=$?
if [ ! $RET_CODE -eq 0 ]; then
  echo "ERROR: failed to prepare file"
  exit 1
fi
echo "Preparing Nucleotide taxID mapping file for BLAST makeblastdb tool"
DATA_FILE="nucl_gb.accession2taxid"
MAPPING_FILE="ncbi-nucl-2columns.accession2taxid"
echo "  $MAPPING_FILE"
tail -n +2 $DATA_FILE | awk '{print $1,"\t",$3}' > $MAPPING_FILE
RET_CODE=$?
if [ ! $RET_CODE -eq 0 ]; then
  echo "ERROR: failed to prepare file"
  exit 1
fi

# Diamond 2.0.6 requires a four column file WITH header
echo "Preparing taxID mapping file for DIAMOND makedb tool"
DATA_FILE="prot.accession2taxid"
MAPPING_FILE="ncbi-prot-4columns.accession2taxid"
ln -s $DATA_FILE $MAPPING_FILE

#echo -e "accession\taccession.version\ttaxid\tgi" > $MAPPING_FILE
# prot.accession2taxid.FULL only contains two columns, so create fake ones
# with skip uts header to avoid bad entry in the output file
#tail -n +2 $DATA_FILE | awk '{print $1,"\t",$1,"\t",$2,"\t0"}' >> $MAPPING_FILE
#RET_CODE=$?
#if [ ! $RET_CODE -eq 0 ]; then
#  echo "ERROR: failed to prepare file"
#  exit 1
#fi

