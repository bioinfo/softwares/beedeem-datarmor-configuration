#!/usr/bin/env bash

# This is a BeeDeeM external task script aims at downloading
# Uniprot IDs mapping file. It also prepares that file toi be
# used to prepare taxonomy-based indexes for BLAST and Diamond.
#
# This script is used in bank descriptor:
#   ../descriptors/Uniprot_XXX.dsc
#
# Such a BeeDeeM script is called by the task engine and
# with arguments as defined in: 
# ./scheduler/common.sh->handleBDMArgs() function

echo "Getting Uniprot ID mapping file"

# ========================================================================================
# Section: include API
S_NAME=$(realpath "$0")
[[ -z "$BDM_CONF_SCRIPTS" ]] && script_dir=$(dirname "$S_NAME") || script_dir=$BDM_CONF_SCRIPTS
. $script_dir/scheduler/common.sh

# ========================================================================================
# Section: handle arguemnts
# Function call setting BDMC_xxx variables from cmdline arguments
handleBDMArgs $@
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Wrong or missing arguments" && exit $RET_CODE

# ========================================================================================
# Section: do business

echo "Changing dir to $BDMC_INST_DIR"
cd $BDMC_INST_DIR

url="ftp://ftp.ebi.ac.uk/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping_selected.tab.gz"
filename="uniprot-idmapping_selected.tab.gz"
ANSWER=$(downloadFile $filename $url)
echo $ANSWER
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Unable to retrieve file" && exit $RET_CODE

echo "Uncompressing $filename..."
CMD="gunzip $filename"
echo $CMD
eval $CMD
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "failed to uncompress file" && exit $RET_CODE

# Create expected two column index file
# (e.g. to be used for Diamond indexing)
MAPPING_FILE="uniprot-4columns.accession2taxid"
echo "Creating Uniprot seqID-taxID mapping file"
filename=${filename%.gz}
echo "Creating $MAPPING_FILE from $filename"
echo -e "accession\taccession.version\ttaxid\tgi" > $MAPPING_FILE
# Note: BeeDeeM reports specific seqIDs in FASTA file, this
#       is why we put $2 instead of $1 in mapping file)
# We use 4 columns format, as it is expected by Diamond makedb
cat $filename | sed -e 's/\t/,/g' | awk -F ',' '{print $2,"\t",$1,"\t",$13,"\t0"}' >> $MAPPING_FILE
RET_CODE=$?
if [ ! $RET_CODE -eq 0 ]; then
  echo "ERROR: failed to make id mapping file"
  exit 1
fi
NLINES=$(wc -l $MAPPING_FILE)
echo "Mapping file contains $NLINES entries"

# Create mapping file expected  by Blast makeblastdb 
MAPPING_FILE="uniprot-2columns.accession2taxid"
echo "Creating $MAPPING_FILE from $filename"
# DO NOT add file header to taxmap file: not expected by makeblastdb
touch $MAPPING_FILE
cat $filename | sed -e 's/\t/,/g' | awk -F ',' '{print $2,"\t",$13}' >> $MAPPING_FILE
RET_CODE=$?
if [ ! $RET_CODE -eq 0 ]; then
  echo "ERROR: failed to make id mapping file"
  exit 1
fi
NLINES=$(wc -l $MAPPING_FILE)
echo "Mapping file contains $NLINES entries"

