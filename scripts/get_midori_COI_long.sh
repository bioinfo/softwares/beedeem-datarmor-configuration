#!/usr/bin/env bash

TARGET_LOCI="COI"
GENE_NAME="CO1"

# This is a BeeDeeM external task script aims at downloading
# MIDORI $TARGET_LOCI longuest DB  using HTTP protocol.
#
# This script is used in bank descriptor:
#   ../descriptors/MIDORI_${TARGET_LOCI}_longuest.dsc
#
# Such a BeeDeeM script is called by the task engine and
# with arguments as defined in: 
# ./scheduler/common.sh->handleBDMArgs() function

echo "Getting MIDORI $TARGET_LOCI longuest"

# ========================================================================================
# Section: include API
S_NAME=$(realpath "$0")
[[ -z "$BDM_CONF_SCRIPTS" ]] && script_dir=$(dirname "$S_NAME") || script_dir=$BDM_CONF_SCRIPTS
. $script_dir/scheduler/common.sh

# ========================================================================================
# Section: handle arguemnts
# Function call setting BDMC_xxx variables from cmdline arguments
handleBDMArgs $@
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Wrong or missing arguments" && exit $RET_CODE

# ========================================================================================
# Section: do business
GB_VER=$(grep "version\=" $script_dir/midori_version | cut -d'=' -f2)
WK_DIR=${BDMC_WK_DIR}/MIDORI_${TARGET_LOCI}_longest
echo "Creating $WK_DIR"
mkdir -p $WK_DIR
echo "Changing dir to $WK_DIR"
cd $WK_DIR

filename="MIDORI2_LONGEST_NUC_GB${GB_VER}_${GENE_NAME}_BLAST.zip"
url="http://www.reference-midori.info/download/Databases/GenBank${GB_VER}/BLAST/longest/$filename"
ANSWER=$(downloadFile $filename $url)
RET_CODE=$?
echo $ANSWER
exit $RET_CODE

