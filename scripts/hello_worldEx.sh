#!/usr/bin/env bash
#PBS -q omp
#PBS -l mem=1g
#PBS -l ncpus=2
#PBS -l walltime=00:10:00
#PBS -j oe
#PBS -N HelloWorld
#PBS -o /home1/scratch/galaxy
#PBS -e /home1/scratch/galaxy

echo "Hello world on OMP queue"

