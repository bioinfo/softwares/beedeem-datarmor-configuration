#!/usr/bin/env bash

# This is a BeeDeeM external task script aims at downloading
# BOLD system sequences using their API.
#
# Doc:
#   https://www.boldsystems.org/index.php/resources/api?type=webservices
# 
# This script is used in bank descriptor:
#   ../descriptors/BOLD_XXX.dsc
#
# Such a BeeDeeM script is called by the task engine and
# with arguments as defined in: 
# ./scheduler/common.sh->handleBDMArgs() function

echo "Getting BOLD system sequences for Alexandrium genus"

# ========================================================================================
# Section: include API
S_NAME=$(realpath "$0")
[[ -z "$BDM_CONF_SCRIPTS" ]] && script_dir=$(dirname "$S_NAME") || script_dir=$BDM_CONF_SCRIPTS
. $script_dir/scheduler/common.sh

# ========================================================================================
# Section: handle arguemnts
# Function call setting BDMC_xxx variables from cmdline arguments
handleBDMArgs $@
RET_CODE=$?
[ ! $RET_CODE -eq 0 ] && errorMsg "Wrong or missing arguments" && exit $RET_CODE

# ========================================================================================
# Section: do business

# use BOLD API accepted expression
#BOLD_TAXON="Alexandrium|Gambierdiscus"
BOLD_TAXON=alexandrium
# Pipe char being restricted in file names, replace it
BOLD_FNAME=`echo $BOLD_TAXON | sed -r 's/\|/_/g'`

#Prepare working directory
BDMC_WK_DIR=${BDMC_WK_DIR}/BOLD_${BOLD_FNAME}
echo "Creating $BDMC_WK_DIR"
mkdir -p $BDMC_WK_DIR
echo "Changing dir to $BDMC_WK_DIR"
cd $BDMC_WK_DIR

# Load data
url="http://www.boldsystems.org/index.php/API_Public/sequence?taxon=${BOLD_TAXON}"
filename="${BOLD_FNAME}.fas"
ANSWER=$(downloadFile $filename $url)
RET_CODE=$?
echo $ANSWER
exit $RET_CODE

